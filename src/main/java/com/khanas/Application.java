package com.khanas;

import com.khanas.parser.Dom;
import com.khanas.parser.Sax;
import com.khanas.parser.Stax;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;

public class Application {

    private static Logger logger = LogManager.getLogger();

    public static void main(final String[] args)
            throws IOException, SAXException,
            ParserConfigurationException, XMLStreamException {

        logger.info("-------Working with Dom--------");
        Dom dom = new Dom();
        dom.printDevices();
        logger.info("Writing to xml");
        dom.writeToXML();

        logger.info("-------Working with Sax--------");
        Sax sax = new Sax();
        sax.printDevices();
        logger.info("Writing to xml");
        sax.writeToXML();

        logger.info("-------Working with Stax-------");
        Stax stax = new Stax();
        stax.printDevices();
        logger.info("Writing to xml");
        stax.writeToXML();
    }
}
