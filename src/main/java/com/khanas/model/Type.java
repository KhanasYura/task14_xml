package com.khanas.model;

public class Type {
    private boolean peripheral;
    private int consumption;
    private String accessories;
    private String ports;

    public Type() {
    }

    public Type(final boolean peripheral, final int consumption,
                final String accessories, final String ports) {
        this.peripheral = peripheral;
        this.consumption = consumption;
        this.accessories = accessories;
        this.ports = ports;
    }

    public final boolean getPeripheral() {
        return peripheral;
    }

    public final void setPeripheral(final boolean peripheral) {
        this.peripheral = peripheral;
    }

    @Override
    public final String toString() {
        return "Type{"
                + "peripheral=" + peripheral
                + ", consumption=" + consumption
                + ", accessories='" + accessories + '\''
                + ", ports='" + ports + '\'' + '}';
    }

    public final int getConsumption() {
        return consumption;
    }

    public final void setConsumption(final int consumption) {
        this.consumption = consumption;
    }

    public final String getAccessories() {
        return accessories;
    }

    public final void setAccessories(final String accessories) {
        this.accessories = accessories;
    }

    public final String getPorts() {
        return ports;
    }

    public final void setPorts(final String ports) {
        this.ports = ports;
    }
}
