package com.khanas.model;

public class Device {
    private String name;
    private String origin;
    private int price;
    private Type type;
    private boolean critical;

    public Device() {
        type = new Type();
    }

    public Device(final String name, final String origin, final int price,
                  final boolean peripheral, final int consumption,
                  final String accessories, final String ports, final boolean critical) {
        this.name = name;
        this.origin = origin;
        this.price = price;
        this.type = new Type(peripheral, consumption, accessories, ports);
        this.critical = critical;
    }

    public final String getName() {
        return name;
    }

    public final void setName(final String name) {
        this.name = name;
    }

    public final String getOrigin() {
        return origin;
    }

    public final void setOrigin(final String origin) {
        this.origin = origin;
    }

    public final int getPrice() {
        return price;
    }

    public final void setPrice(final int price) {
        this.price = price;
    }

    public final Type getType() {
        return type;
    }

    public final void setType(final Type type) {
        this.type = type;
    }

    public final boolean getCritical() {
        return critical;
    }

    public final void setCritical(final boolean critical) {
        this.critical = critical;
    }

    @Override
    public final String toString() {
        return "Device{"
                + "name='" + name + '\''
                + ", origin='" + origin + '\''
                + ", price=" + price
                + ", type=" + type
                + ", critical=" + critical + '}';
    }
}
