package com.khanas.parser;

import com.khanas.model.Device;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static com.khanas.parser.Validation.validateXMLSchema;

public class Dom {

    private ArrayList<Device> devices = new ArrayList<>();
    private Device device;

    public Dom() throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File("src/main/resources/Devices.xml"));
        if (validateXMLSchema("src/main/resources/DevicesXSD.xsd", "src/main/resources/Devices.xml")) {
            Element element = document.getDocumentElement();
            getDevices(element.getChildNodes());
        } else {
            System.out.println("Validation fault");
        }
    }

    public final void getDevices(final NodeList list) {
        for (int i = 0; i < list.getLength(); i++) {
            if (list.item(i) instanceof Element) {
                if (!list.item(i).getTextContent().trim().isEmpty()
                        && !((Text) list.item(i).getFirstChild()).getData().trim().isEmpty()) {
                    String value = ((Text) list.item(i).getFirstChild()).getData().trim();
                    if (((Element) list.item(i)).getTagName().equalsIgnoreCase("name")) {
                        device = new Device();
                        device.setName(value);
                    } else if (((Element) list.item(i)).getTagName().equalsIgnoreCase("origin")) {
                        device.setOrigin(value);
                    } else if (((Element) list.item(i)).getTagName().equalsIgnoreCase("price")) {
                        device.setPrice(Integer.parseInt(value));
                    } else if (((Element) list.item(i)).getTagName().equalsIgnoreCase("peripheral")) {
                        device.getType().setPeripheral(Boolean.parseBoolean(value));
                    } else if (((Element) list.item(i)).getTagName().equalsIgnoreCase("consumption")) {
                        device.getType().setConsumption(Integer.parseInt(value));
                    } else if (((Element) list.item(i)).getTagName().equalsIgnoreCase("accessories")) {
                        device.getType().setAccessories(value);
                    } else if (((Element) list.item(i)).getTagName().equalsIgnoreCase("ports")) {
                        device.getType().setPorts(value);
                    } else if (((Element) list.item(i)).getTagName().equalsIgnoreCase("critical")) {
                        device.setCritical(Boolean.parseBoolean(value));
                        devices.add(device);
                    }
                }

                if (list.item(i).hasChildNodes()) {
                    getDevices(list.item(i).getChildNodes());
                }
            }
        }
    }

    public final void printDevices() {
        deviceSorted();
        for (Device device : devices) {
            System.out.println(device.toString());
        }
    }

    public final void deviceSorted() {
        Comparator<Device> comparator = new Comparator<Device>() {
            @Override
            public int compare(final Device o1, final Device o2) {
                return o1.getName().compareTo(o2.getName());
            }
        };
        Collections.sort(devices, comparator);
    }

    public final void writeToXML() {
        new WriteToXML(devices, "DomXML");
    }
}

