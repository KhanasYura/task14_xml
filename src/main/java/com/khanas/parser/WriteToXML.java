package com.khanas.parser;

import com.khanas.model.Device;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.util.ArrayList;

public class WriteToXML {

    private ArrayList<Device> devices;

    public WriteToXML(final ArrayList<Device> devices, final String xmlName) {
        this.devices = devices;
        doit(xmlName);
    }

    public final void doit(final String xmlName) {
        try {
            Document xmldoc = initXML();
            Element root = xmldoc.getDocumentElement();

            process(xmldoc, root);

            Node pi = xmldoc.createProcessingInstruction
                    ("xml-stylesheet",
                            "type=\"text/xsl\" href=\"DevicesXSLT.xsl\"");
            xmldoc.insertBefore(pi, root);

            StreamResult out = new StreamResult(xmlName + ".xml");
            writeXML(xmldoc, out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final Document initXML() throws ParserConfigurationException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation impl = builder.getDOMImplementation();
        return impl.createDocument(null, "devices", null);
    }

    public final void process(final Document xmldoc, final Element root) {

        for (int i = 0; i < devices.size(); i++) {
            Element device = xmldoc.createElement("device");
            device.setAttribute("deviceNo", String.valueOf(i));

            Element name = xmldoc.createElement("name");
            Node textName = xmldoc.createTextNode(devices.get(i).getName());
            name.appendChild(textName);
            Element origin = xmldoc.createElement("origin");
            Node textOrigin = xmldoc.createTextNode(devices.get(i).getOrigin());
            origin.appendChild(textOrigin);
            Element price = xmldoc.createElement("price");
            Node textPrice = xmldoc.createTextNode(String.valueOf(devices.get(i)
                    .getPrice()));
            price.appendChild(textPrice);

            Element type = xmldoc.createElement("type");

            Element peripheral = xmldoc.createElement("peripheral");
            Node textPeripheral = xmldoc.createTextNode(String.valueOf(devices
                    .get(i).getType().getPeripheral()));
            peripheral.appendChild(textPeripheral);
            Element consumption = xmldoc.createElement("consumption");
            Node textConsumption = xmldoc.createTextNode(String.valueOf(devices
                    .get(i).getType().getConsumption()));
            consumption.appendChild(textConsumption);
            Element accessories = xmldoc.createElement("accessories");
            Node textAccessories = xmldoc.createTextNode(devices
                    .get(i).getType().getAccessories());
            accessories.appendChild(textAccessories);
            Element ports = xmldoc.createElement("ports");
            Node textPorts = xmldoc.createTextNode(devices
                    .get(i).getType().getPorts());
            ports.appendChild(textPorts);

            type.appendChild(peripheral);
            type.appendChild(consumption);
            type.appendChild(accessories);
            type.appendChild(ports);

            Element critical = xmldoc.createElement("critical");
            Node textCritical = xmldoc.createTextNode(String.valueOf(devices
                    .get(i).getCritical()));
            critical.appendChild(textCritical);

            device.appendChild(name);
            device.appendChild(origin);
            device.appendChild(price);
            device.appendChild(type);
            device.appendChild(critical);
            root.appendChild(device);
        }

    }

    public final void writeXML(final Document xmldoc, final StreamResult out)
            throws TransformerException {
        DOMSource domSource = new DOMSource(xmldoc);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        transformer.transform(domSource, out);
    }
}
