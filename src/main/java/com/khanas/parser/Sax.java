package com.khanas.parser;

import com.khanas.model.Device;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static com.khanas.parser.Validation.validateXMLSchema;

public class Sax {

    private ArrayList<Device> devices = new ArrayList<>();
    private Device device;
    private boolean name;
    private boolean origin;
    private boolean price;
    private boolean peripheral;
    private boolean consumption;
    private boolean accessories;
    private boolean ports;
    private boolean critical;

    public Sax() throws ParserConfigurationException, SAXException, IOException {

        if (validateXMLSchema("src/main/resources/DevicesXSD.xsd",
                "src/main/resources/Devices.xml")) {
            getDevices();
        } else {
            System.out.println("Validation fault");
        }
    }

    public final void getDevices() throws ParserConfigurationException, SAXException, IOException {
        DefaultHandler hander = new DefaultHandler() {
            @Override
            public void characters(final char[] ch, final int start,final int length) {
                String str = "";
                for (int i = 0; i < length; i++) {
                    str += ch[start + i];
                }
                if (!str.trim().isEmpty()) {
                    if (name) {
                        device = new Device();
                        device.setName(str);
                        name = false;
                    }
                    if (origin) {
                        device.setOrigin(str);
                        origin = false;
                    }
                    if (price) {
                        device.setPrice(Integer.parseInt(str));
                        price = false;
                    }
                    if (peripheral) {
                        device.getType().setPeripheral(Boolean.parseBoolean(str));
                        peripheral = false;
                    }
                    if (consumption) {
                        device.getType().setConsumption(Integer.parseInt(str));
                        consumption = false;
                    }
                    if (accessories) {
                        device.getType().setAccessories(str);
                        accessories = false;
                    }
                    if (ports) {
                        device.getType().setPorts(str);
                        ports = false;
                    }
                    if (critical) {
                        device.setCritical(Boolean.parseBoolean(str));
                        critical = false;
                        devices.add(device);
                    }
                }
            }

            @Override
            public void startElement(final String uri, final String localName,
                                     final String qName, final Attributes attributes)
                    throws SAXException {
                if (qName.equalsIgnoreCase("name")) {
                    name = true;
                }
                if (qName.equalsIgnoreCase("origin")) {
                    origin = true;
                }
                if (qName.equalsIgnoreCase("price")) {
                    price = true;
                }
                if (qName.equalsIgnoreCase("peripheral")) {
                    peripheral = true;
                }
                if (qName.equalsIgnoreCase("consumption")) {
                    consumption = true;
                }
                if (qName.equalsIgnoreCase("accessories")) {
                    accessories = true;
                }
                if (qName.equalsIgnoreCase("ports")) {
                    ports = true;
                }
                if (qName.equalsIgnoreCase("critical")) {
                    critical = true;
                }
            }
        };

        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = factory.newSAXParser();
        parser.parse(new File("src/main/resources/Devices.xml"), hander);
    }

    public final void printDevices() {
        deviceSorted();
        for (Device device : devices) {
            System.out.println(device.toString());
        }
    }

    public final void deviceSorted() {
        Comparator<Device> comparator = (o1, o2) -> o1.getName().compareTo(o2.getName());
        Collections.sort(devices, comparator);
    }

    public final void writeToXML() {
        new WriteToXML(devices, "SaxXML");
    }
}
