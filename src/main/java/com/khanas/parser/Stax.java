package com.khanas.parser;

import com.khanas.model.Device;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static com.khanas.parser.Validation.validateXMLSchema;

public class Stax {

    private ArrayList<Device> devices = new ArrayList<>();
    private Device device;

    public Stax() throws FileNotFoundException, XMLStreamException {
        if (validateXMLSchema("src/main/resources/DevicesXSD.xsd",
                "src/main/resources/Devices.xml")) {
            getDevices();
        } else {
            System.out.println("Validation fault");
        }
    }

    public final void getDevices() throws XMLStreamException, FileNotFoundException {
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader parser = factory.createXMLStreamReader(
                new FileInputStream("src/main/resources/Devices.xml"));
        while (parser.hasNext()) {
            int event = parser.next();
            if (event == XMLStreamConstants.START_ELEMENT) {
                if (parser.getLocalName().equalsIgnoreCase("name")) {
                    parser.next();
                    device = new Device();
                    device.setName(parser.getText().trim());
                } else if (parser.getLocalName().equalsIgnoreCase("origin")) {
                    parser.next();
                    assert device != null;
                    device.setOrigin(parser.getText().trim());
                } else if (parser.getLocalName().equalsIgnoreCase("price")) {
                    parser.next();
                    assert device != null;
                    device.setPrice(Integer.parseInt(parser.getText().trim()));
                } else if (parser.getLocalName().equalsIgnoreCase("peripheral")) {
                    parser.next();
                    assert device != null;
                    device.getType().setPeripheral(Boolean.parseBoolean(parser.getText().trim()));
                } else if (parser.getLocalName().equalsIgnoreCase("consumption")) {
                    parser.next();
                    assert device != null;
                    device.getType().setConsumption(Integer.parseInt(parser.getText().trim()));
                } else if (parser.getLocalName().equalsIgnoreCase("accessories")) {
                    parser.next();
                    assert device != null;
                    device.getType().setAccessories(parser.getText().trim());
                } else if (parser.getLocalName().equalsIgnoreCase("ports")) {
                    parser.next();
                    assert device != null;
                    device.getType().setPorts(parser.getText().trim());
                } else if (parser.getLocalName().equalsIgnoreCase("critical")) {
                    parser.next();
                    assert device != null;
                    device.setCritical(Boolean.parseBoolean(parser.getText().trim()));
                    devices.add(device);
                }
            }
        }
    }

    public final void printDevices() {
        deviceSorted();
        for (Device device : devices) {
            System.out.println(device.toString());
        }
    }

    public final void deviceSorted() {
        Comparator<Device> comparator = (o1, o2) -> o1.getName().compareTo(o2.getName());
        Collections.sort(devices, comparator);
    }

    public final void writeToXML() {
        new WriteToXML(devices, "StaxXML");
    }
}
