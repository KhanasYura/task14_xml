<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <body style="font-family: Arial;
                         font-size: 12pt;
                         background-color: #EEE;">
                <div style="background-color: green;
                            color:black;">
                    <h1>Devices</h1>
                </div>
                <table border="2">
                    <tr bgcolor="#2E9AFE">
                        <th>Name</th>
                        <th>Origin</th>
                        <th>Price</th>
                        <th>Type</th>
                        <th>Critical</th>
                    </tr>
                    <xsl:for-each select="devices/device">
                        <tr>
                            <td><xsl:value-of select="name"/></td>
                            <td><xsl:value-of select="origin"/></td>
                            <td><xsl:value-of select="price"/></td>
                            <td>
                                Peripheral: <xsl:value-of select="type/peripheral"/>;
                                Consumption: <xsl:value-of select="type/consumption"/> watt;
                                Accessories: <xsl:value-of select="type/accessories"/>;
                                Ports: <xsl:value-of select="type/ports"/>;
                            </td>
                            <td><xsl:value-of select="critical"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>